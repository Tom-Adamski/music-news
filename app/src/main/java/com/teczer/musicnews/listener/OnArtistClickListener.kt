package com.teczer.musicnews.listener

import com.teczer.musicnews.database.entity.Artist

interface OnArtistClickListener {
    fun onArtistClick(artist: Artist, action: ArtistAction)

    enum class ArtistAction {
        SUBSCRIBE, REMOVE, SHOW
    }
}