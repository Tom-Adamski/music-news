package com.teczer.musicnews.ui.main

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.teczer.musicnews.R
import com.teczer.musicnews.SongActivity
import com.teczer.musicnews.adapter.SongsAdapter
import com.teczer.musicnews.container.NestedSong
import com.teczer.musicnews.database.AppDatabase
import com.teczer.musicnews.database.dao.ArtistDao
import com.teczer.musicnews.database.dao.ContributionDao
import com.teczer.musicnews.database.dao.SongDao
import com.teczer.musicnews.database.entity.Song
import com.teczer.musicnews.listener.OnSongClickListener
import kotlinx.android.synthetic.main.fragment_feed.view.*
import java.lang.Exception
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * A placeholder fragment containing a simple view.
 */
class FeedFragment(val behavior: Behavior) : Fragment(), OnSongClickListener {

    private lateinit var adapter: SongsAdapter

    private lateinit var db: AppDatabase
    private lateinit var songDao: SongDao
    private lateinit var artistDao: ArtistDao
    private lateinit var contributionDao: ContributionDao

    var artistId = -1

    lateinit var preferences: SharedPreferences

    private var showOnUnknownDate = false

    val geniusDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
    lateinit var localDateFormat: DateFormat

    var mSongs: ArrayList<NestedSong> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_feed, container, false)

        db = AppDatabase.getAppDatabase(activity!!)
        songDao = db.songDao()
        artistDao = db.artistDao()
        contributionDao = db.contributionDao()

        adapter = SongsAdapter(context!!, this, mSongs)

        root.recycler_view.adapter = adapter
        root.recycler_view.layoutManager = LinearLayoutManager(context!!)
        root.recycler_view.setHasFixedSize(true)
        root.recycler_view.setItemViewCacheSize(20)

        localDateFormat = android.text.format.DateFormat.getDateFormat(context)

        preferences = context!!.getSharedPreferences("test", Activity.MODE_PRIVATE)

        showOnUnknownDate =
            preferences.getBoolean(getString(R.string.unknown_date_settings_pref), false)

        AsyncTask.execute {

            //Get today's date as genius format
            val currentDate = geniusDateFormat.format(Date())

            val songs: ArrayList<Song> = ArrayList()

            when (behavior) {
                Behavior.OUT -> {
                    songs.addAll(songDao.getAllUntilDate(currentDate))
                }
                Behavior.UPCOMING -> {
                    songs.addAll(songDao.getAllAfterDate(currentDate))
                }
                Behavior.ARTIST -> {
                    if (artistId != -1) {
                        val contributionsIds = contributionDao.getAllIdsForArtist(artistId)

                        contributionsIds.forEach {id ->
                            val song = songDao.getById(id)
                            if (song != null) {
                                songs.add(song)
                            }
                        }

                        songs.sortBy { it.date }
                        songs.reverse()

                    }
                }
                Behavior.UNKNOWN -> {
                    songs.clear()
                }
                Behavior.NEW -> {
                    songs.addAll(songDao.getAllUntilDate(currentDate))
                }
            }

            songs.forEach { s ->

                //Show a song only if its release date is known or the user accepts unknown dates
                if (showOnUnknownDate || s.date != getString(R.string.unknown_date)) {

                    val songDate = try {
                        localDateFormat.format(geniusDateFormat.parse(s.date)!!)
                    } catch (e : Exception) {
                        getString(R.string.unknown_date)
                    }


                    val nestedSong = NestedSong(s.id, s.title, s.imageUrl, songDate)

                    var artistIds = intArrayOf()

                    contributionDao.getAllForSong(s.id).forEach { c ->

                        if (!artistIds.contains(c.artistId)) {
                            artistIds = intArrayOf(*artistIds, c.artistId)
                            val artist = artistDao.loadById(c.artistId)
                            if (artist != null && artist.subscribed) {
                                nestedSong.add(artist.imageUrl)
                            }
                        }
                    }

                    activity?.runOnUiThread {
                        mSongs.add(nestedSong)
                        adapter.notifyItemInserted(mSongs.size - 1)
                    }
                }
            }
        }


        return root
    }


    override fun onSongClick(position: Int) {
        val id = mSongs[position].id
        val intent = Intent(context, SongActivity::class.java)
        intent.putExtra(getString(R.string.extra_song_id), id)
        startActivity(intent)
    }

    override fun onLongSongClick(position: Int) {
        val id = mSongs[position].id

        AlertDialog.Builder(context)
            .setTitle("Delete song")
            .setMessage("Are you sure you want to delete this song ?")
            .setNegativeButton("Keep", null)
            .setNeutralButton("Hide") { dialog, _ ->
                hideSong(id)
                mSongs.removeAt(position)
                adapter.notifyItemRemoved(position)
            }
            .setPositiveButton("Delete") { dialog, _ ->
                deleteSong(id)
                mSongs.removeAt(position)
                adapter.notifyItemRemoved(position)
            }
            .show()

    }

    private fun hideSong(id: Int) {
        AsyncTask.execute {
            songDao.hide(id)
        }
    }

    private fun deleteSong(id: Int) {
        AsyncTask.execute {
            songDao.delete(id)
        }
    }

    enum class Behavior {
        OUT, UPCOMING, UNKNOWN, ARTIST, NEW
    }

}