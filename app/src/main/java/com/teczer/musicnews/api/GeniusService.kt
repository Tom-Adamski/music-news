package com.teczer.musicnews.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GeniusService {

    @GET("search/artist")
    fun artists(
        @Query("q") query : String,
        @Query("page") page : Int = 1
    ) : Call<ArtistApi>

    @GET("artists/{id}/songs")
    fun releases(
        @Path("id") id : Int,
        @Query("page") page : Int = 1,
        @Query("sort") sort : String = "release_date"
    ) : Call<ReleaseApi>

    @GET("songs/{id}")
    fun song(
        @Path("id") id : Int,
        @Query("access_token") accessToken : String = ACCESS_TOKEN
    ) : Call<SongApi>

    companion object {
        const val ACCESS_TOKEN : String  = "S0vCfM0Pfq1zQbUqiyqb82d9XlT3Nzf0pDea6f_IlZm3FF596UR2RQ4__dEm-Y-b"
    }

}