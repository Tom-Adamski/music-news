package com.teczer.musicnews.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.teczer.musicnews.R
import com.teczer.musicnews.container.NestedSong
import com.teczer.musicnews.listener.OnSongClickListener
import kotlinx.android.synthetic.main.bubble_artist.view.*
import kotlinx.android.synthetic.main.recycler_song_item.view.*

class SongsAdapter(private val context: Context, private val listener: OnSongClickListener, private val mSongs : ArrayList<NestedSong>) :
    RecyclerView.Adapter<SongsAdapter.SongHolder>() {

    private val mInflater = LayoutInflater.from(context)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongHolder {
        val itemView = mInflater.inflate(R.layout.recycler_song_item, parent, false)
        return SongHolder(itemView, listener)
    }

    override fun onBindViewHolder(holder: SongHolder, position: Int) {
        if (mSongs.size > 0) {

            val song = mSongs[position]

            holder.id = song.id

            holder.title.text = song.title
            holder.date.text = song.date

            Glide.with(context)
                .load(song.imageUrl)
                .placeholder(R.drawable.ic_album_black_24dp)
                .into(holder.songImage)

            holder.artists.removeAllViews()

            song.getArtistPics().forEach { url ->

                val container = RelativeLayout(context)

                val artistView =
                    LayoutInflater.from(context).inflate(R.layout.bubble_artist, container)
                val imageView = artistView.artist

                Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.ic_person_black_24dp)
                    .apply(RequestOptions.circleCropTransform())
                    .into(imageView)

                holder.artists.addView(container)
            }


        }
    }

    override fun getItemCount(): Int {
        return mSongs.size
    }

    class SongHolder(itemView: View, val listener: OnSongClickListener) :
        RecyclerView.ViewHolder(itemView) {

        val songImage = itemView.picture
        val title = itemView.title
        val date = itemView.date
        val artists = itemView.artists
        var id: Int = -1

        init {
            itemView.setOnClickListener {
                listener.onSongClick(adapterPosition)
            }
            itemView.setOnLongClickListener {
                listener.onLongSongClick(adapterPosition)
                true
            }
        }

    }


}