package com.teczer.musicnews.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.teczer.musicnews.R

private val TAB_TITLES = arrayOf(
    R.string.tab_text_0,
    R.string.tab_text_1,
    R.string.tab_text_2,
    R.string.tab_text_3,
    R.string.tab_text_4
)

class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm) {

    private val feedUpcomingFragment = FeedFragment(FeedFragment.Behavior.UPCOMING)
    private val feedOutFragment = FeedFragment(FeedFragment.Behavior.OUT)
    private val searchFragment = SearchFragment()
    private val artistsFragment = ArtistsFragment()
    private val settingsFragment = SettingsFragment()

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                feedUpcomingFragment
            }
            1 -> {
                feedOutFragment
            }
            2 -> {
                searchFragment
            }
            3 -> {
                artistsFragment
            }
            4 -> {
                settingsFragment
            }
            else -> {
                Fragment()
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return TABS_COUNT
    }


    companion object {
        const val TABS_COUNT = 5
    }


}