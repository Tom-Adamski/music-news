package com.teczer.musicnews.container

class NestedSong(val id : Int, val title: String, val imageUrl : String, val date: String) {

    private val artistPics = ArrayList<String>()

    fun add(url : String){
        artistPics.add(url)
    }

    fun getArtistPics() : ArrayList<String>{
        return artistPics
    }

}