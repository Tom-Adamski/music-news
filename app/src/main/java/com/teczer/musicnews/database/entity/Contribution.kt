package com.teczer.musicnews.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(primaryKeys = ["song_id", "artist_id", "role"])
data class Contribution(
    @ColumnInfo(name = "song_id")
    val songId : Int,
    @ColumnInfo(name = "artist_id")
    val artistId : Int,
    @ColumnInfo(name = "role")
    val role : String
){
    companion object {
        enum class Role(name : String){
            RolePrimary("RolePrimary"),
            RoleProducer("RoleProducer"),
            RoleFeaturing("RoleFeaturing"),
            RoleWriter("RoleWriter")
        }
    }
}