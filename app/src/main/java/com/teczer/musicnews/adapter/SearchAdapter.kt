package com.teczer.musicnews.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.teczer.musicnews.R
import com.teczer.musicnews.database.entity.Artist
import com.teczer.musicnews.listener.OnArtistClickListener
import kotlinx.android.synthetic.main.recycler_artist_item.view.name
import kotlinx.android.synthetic.main.recycler_artist_item.view.picture
import kotlinx.android.synthetic.main.recycler_search_item.view.*

class SearchAdapter(private val context : Context, private val listener: OnArtistClickListener) : RecyclerView.Adapter<SearchAdapter.ArtistHolder>() {

    private val mInflater = LayoutInflater.from(context)

    private var mArtists = ArrayList<Artist>()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArtistHolder {
        val itemView = mInflater.inflate(R.layout.recycler_search_item, parent, false)
        return ArtistHolder(itemView, listener)
    }

    override fun onBindViewHolder(holder: ArtistHolder, position: Int) {
        if(mArtists.size > 0 ){
            holder.artist = mArtists[position]
            holder.artistName.text = holder.artist!!.name
            holder.id = holder.artist!!.id

            val url = holder.artist!!.imageUrl

            Glide.with(context)
                .load(url)
                .placeholder(R.drawable.ic_person_black_24dp)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.artistImage)


        } else {
            holder.artistName.text = "Empty"
        }
    }

    override fun getItemCount(): Int {
        return mArtists.size
    }

    fun empty(){
        mArtists = ArrayList()
        notifyDataSetChanged()
    }

    fun addArtist(artist : Artist) {
        mArtists.add(artist)
        notifyItemInserted(mArtists.size -1)
    }

    class ArtistHolder(itemView : View, listener: OnArtistClickListener) : RecyclerView.ViewHolder(itemView){
        var artist : Artist? = null

        val artistImage = itemView.picture
        val artistName = itemView.name
        var id : Int? = null

        init{
            itemView.sub.setOnClickListener {
                listener.onArtistClick(artist!!, OnArtistClickListener.ArtistAction.SUBSCRIBE)
            }
        }


    }

}
