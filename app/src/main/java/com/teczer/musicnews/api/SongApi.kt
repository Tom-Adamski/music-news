package com.teczer.musicnews.api

data class SongApi(
    val response: SongResponse
)

data class SongResponse(
    val song: Song
)

data class Song(
    val id : Int,
    val title : String,
    val song_art_image_url : String,
    val primary_artist : Artist,
    val featured_artists : List<Artist>?,
    val producer_artists : List<Artist>?,
    val writer_artists : List<Artist>?,
    val media : List<Media>,
    val release_date : String?
)

data class Artist(
    val id : Int,
    val name : String,
    val image_url : String
)

data class Media(
    val provider : String,
    val url : String
)