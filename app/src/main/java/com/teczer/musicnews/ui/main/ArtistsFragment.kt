package com.teczer.musicnews.ui.main

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.teczer.musicnews.ArtistActivity
import com.teczer.musicnews.R
import com.teczer.musicnews.adapter.ArtistsAdapter
import com.teczer.musicnews.database.AppDatabase
import com.teczer.musicnews.database.dao.ArtistDao
import com.teczer.musicnews.database.entity.Artist
import com.teczer.musicnews.listener.OnArtistClickListener
import kotlinx.android.synthetic.main.fragment_search.view.*

/**
 * A placeholder fragment containing a simple view.
 */
class ArtistsFragment : Fragment(), OnArtistClickListener {

    private lateinit var adapter : ArtistsAdapter

    private lateinit var db : AppDatabase
    private lateinit var artistDao: ArtistDao

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_artists, container, false)

        db = AppDatabase.getAppDatabase(activity!!)
        artistDao = db.artistDao()

        adapter = ArtistsAdapter(context!!, this)

        root.recycler_view.adapter = adapter
        root.recycler_view.layoutManager = LinearLayoutManager(context!!)
        root.recycler_view.setHasFixedSize(true)
        root.recycler_view.setItemViewCacheSize(20)

        AsyncTask.execute {
            artistDao.getAllSubscribed().forEach {
                adapter.addArtist(it)
            }
        }


        return root
    }


    override fun onArtistClick(artist: Artist, action: OnArtistClickListener.ArtistAction) {

        when(action){
            OnArtistClickListener.ArtistAction.REMOVE -> {
                adapter.removeArtist(artist)
                AsyncTask.execute {
                    val unsub = Artist(artist.id, artist.name, artist.imageUrl, false)
                    artistDao.unsubscribe(unsub)
                }
            }
            OnArtistClickListener.ArtistAction.SHOW -> {
                val intent = Intent(context, ArtistActivity::class.java)
                intent.putExtra(getString(R.string.extra_artist_id), artist.id)
                intent.putExtra(getString(R.string.extra_artist_name), artist.name)
                startActivity(intent)
            }
        }


    }


}