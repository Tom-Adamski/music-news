package com.teczer.musicnews.database.dao

import androidx.room.*
import com.teczer.musicnews.database.entity.Contribution

@Dao
interface ContributionDao {

    @Query("SELECT * FROM contribution")
    fun getAll() : List<Contribution>

    @Query("SELECT * FROM contribution WHERE song_id = :songId")
    fun getAllForSong(songId : Int) : List<Contribution>

    @Query("SELECT * FROM contribution WHERE artist_id = :artistId")
    fun getAllForArtist(artistId : Int) : List<Contribution>

    @Query("SELECT DISTINCT song_id FROM contribution WHERE artist_id = :artistId")
    fun getAllIdsForArtist(artistId : Int) : List<Int>

    @Query("DELETE FROM contribution WHERE song_id = :songId")
    fun deleteForSongId(songId : Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(contribution: Contribution) : Long

    @Delete
    fun delete(contribution: Contribution)

    @Query("DELETE FROM Contribution")
    fun purge()

}