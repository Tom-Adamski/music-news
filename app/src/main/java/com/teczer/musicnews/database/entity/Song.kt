package com.teczer.musicnews.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Song(
    @PrimaryKey
    val id : Int,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "image_url")
    val imageUrl : String,
    @ColumnInfo(name = "date")
    val date: String,
    @ColumnInfo(name = "hidden")
    var hidden : Boolean = false,
    @ColumnInfo(name = "updated")
    var updated : Int = 0,
    @ColumnInfo(name = "state")
    var state : Int = 0
) {

    enum class State(val code : Int){
        SEEN(0),
        UPDATED(1),
        NEW(2)
    }

}