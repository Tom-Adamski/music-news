package com.teczer.musicnews

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.teczer.musicnews.ui.main.FeedFragment
import kotlinx.android.synthetic.main.activity_artist.*

class ArtistActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist)

        val artistId = intent.getIntExtra(getString(R.string.extra_artist_id), -1)
        val artistName = intent.getStringExtra(getString(R.string.extra_artist_name))

        artist_name.text = artistName

        val feedFragment = FeedFragment(FeedFragment.Behavior.ARTIST)
        feedFragment.artistId = artistId

        val transaction = supportFragmentManager.beginTransaction()

        transaction.add(R.id.song_list_container, feedFragment)

        transaction.commit()



    }
}
