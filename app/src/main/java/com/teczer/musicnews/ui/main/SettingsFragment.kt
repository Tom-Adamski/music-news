package com.teczer.musicnews.ui.main

import android.app.Activity
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.teczer.musicnews.R
import com.teczer.musicnews.database.AppDatabase
import com.teczer.musicnews.database.dao.ContributionDao
import com.teczer.musicnews.database.dao.SongDao
import com.teczer.musicnews.workers.SyncWorker
import kotlinx.android.synthetic.main.fragment_settings.view.*
import kotlinx.android.synthetic.main.fragment_settings.view.purge_songs

class SettingsFragment : Fragment() {

    lateinit var preferences: SharedPreferences

    private lateinit var db : AppDatabase
    private lateinit var songDao : SongDao
    private lateinit var contributionDao: ContributionDao

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_settings, container, false)

        db = AppDatabase.getAppDatabase(context!!)

        songDao = db.songDao()
        contributionDao = db.contributionDao()

        preferences = context!!.getSharedPreferences("test", Activity.MODE_PRIVATE)

        val isChecked =
            preferences.getBoolean(getString(R.string.unknown_date_settings_pref), false)
        root.unknown_date_switch.isChecked = isChecked

        root.unknown_date_switch.setOnCheckedChangeListener { _, state ->
            preferences.edit().putBoolean(getString(R.string.unknown_date_settings_pref), state)
                .apply()

        }

        root.worker_start.setOnClickListener {
            val releaseWorker = OneTimeWorkRequestBuilder<SyncWorker>().build()
            WorkManager.getInstance(context!!).enqueue(releaseWorker)
        }

        root.purge_songs.setOnClickListener {
            AsyncTask.execute {
                songDao.purge()
                contributionDao.purge()
            }
        }

        return root
    }

}