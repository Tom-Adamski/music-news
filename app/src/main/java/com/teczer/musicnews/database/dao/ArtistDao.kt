package com.teczer.musicnews.database.dao

import androidx.room.*
import com.teczer.musicnews.database.entity.Artist

@Dao
interface ArtistDao {

    @Query("SELECT * FROM artist ORDER BY name ASC")
    fun getAll() : List<Artist>

    @Query("SELECT * FROM artist WHERE subscribed ORDER BY name ASC")
    fun getAllSubscribed() : List<Artist>

    @Query("SELECT * FROM artist WHERE id = :artistId LIMIT 1")
    fun loadById(artistId: Int): Artist?

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(artist: Artist) : Long

    @Query("UPDATE artist SET subscribed = 1 WHERE id = :id")
    fun setSubscribed(id : Int)

    @Query("UPDATE artist SET subscribed = 0 WHERE id = :id")
    fun setUnsubscribed(id : Int)


    fun subscribe(artist: Artist){
        if(insert(artist) == -1L){
            setSubscribed(artist.id)
        }
    }

    fun unsubscribe(artist: Artist){
        if(insert(artist) == -1L){
            setUnsubscribed(artist.id)
        }
    }


    @Delete
    fun delete(artist: Artist)


}