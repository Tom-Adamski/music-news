package com.teczer.musicnews.api

data class ArtistApi(
    val response: ArtistResponse
)

data class ArtistResponse(
    val sections: List<ArtistSection>
)

data class ArtistSection(
    val hits : List<ArtistHit>
)

data class ArtistHit(
    val result : ArtistResult
)

data class ArtistResult(
    val id : Int,
    val name : String,
    val image_url : String
)