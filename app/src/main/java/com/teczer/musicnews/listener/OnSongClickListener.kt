package com.teczer.musicnews.listener

interface OnSongClickListener {

    fun onSongClick(position : Int)

    fun onLongSongClick(position : Int)

}