package com.teczer.musicnews.database.dao

import androidx.room.*
import com.teczer.musicnews.database.entity.Song

@Dao
interface SongDao {

    @Query("SELECT * FROM song ORDER BY date DESC")
    fun getAll() : List<Song>

    @Query("SELECT * FROM song WHERE date > :date ORDER BY date ASC")
    fun getAllAfterDate(date: String) : List<Song>

    @Query("SELECT * FROM song WHERE date <= :date ORDER BY date DESC")
    fun getAllUntilDate(date: String) : List<Song>

    @Query("SELECT * FROM song WHERE state = 2 ORDER BY date DESC")
    fun getNew() : List<Song>

    @Query("SELECT * FROM song WHERE id = :songId LIMIT 1")
    fun getById(songId : Int) : Song?

    @Query("UPDATE song SET hidden = 1 WHERE id = :songId")
    fun hide(songId : Int) : Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(song: Song) : Long

    @Query("DELETE FROM SONG WHERE id = :id")
    fun delete(id: Int)

    @Query("DELETE FROM Song")
    fun purge()


}