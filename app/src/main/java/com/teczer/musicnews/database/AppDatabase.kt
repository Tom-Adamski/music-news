package com.teczer.musicnews.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.teczer.musicnews.database.dao.ArtistDao
import com.teczer.musicnews.database.dao.ContributionDao
import com.teczer.musicnews.database.dao.SongDao
import com.teczer.musicnews.database.entity.Artist
import com.teczer.musicnews.database.entity.Contribution
import com.teczer.musicnews.database.entity.Song

@Database(entities = [Artist::class, Song::class, Contribution::class], version = 2)
abstract class AppDatabase : RoomDatabase() {

    abstract fun artistDao(): ArtistDao
    abstract fun songDao(): SongDao
    abstract fun contributionDao(): ContributionDao


    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getAppDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "music-news-database"
                )
                    .addMigrations(MIGRATION_1_2)
                    .build()
            }
            return INSTANCE as AppDatabase
        }

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE Song ADD COLUMN hidden INTEGER DEFAULT 0 NOT NULL")
            }
        }
    }

}