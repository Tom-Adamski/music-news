package com.teczer.musicnews

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.teczer.musicnews.ui.main.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        view_pager.adapter = sectionsPagerAdapter
        view_pager.offscreenPageLimit = SectionsPagerAdapter.TABS_COUNT
        tabs.setupWithViewPager(view_pager)
    }
}