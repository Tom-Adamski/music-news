package com.teczer.musicnews.workers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.teczer.musicnews.R
import com.teczer.musicnews.SongActivity
import com.teczer.musicnews.api.GeniusService
import com.teczer.musicnews.api.ReleaseApi
import com.teczer.musicnews.api.SongApi
import com.teczer.musicnews.database.AppDatabase
import com.teczer.musicnews.database.dao.ArtistDao
import com.teczer.musicnews.database.dao.ContributionDao
import com.teczer.musicnews.database.dao.SongDao
import com.teczer.musicnews.database.entity.Artist
import com.teczer.musicnews.database.entity.Contribution
import com.teczer.musicnews.database.entity.Song
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class SyncWorker(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {

    private lateinit var db: AppDatabase
    private lateinit var artistDao: ArtistDao
    private lateinit var songDao: SongDao
    private lateinit var contributionDao: ContributionDao

    private val databaseSongs = HashMap<Int, Int>()
    private val toAddSongs = HashMap<Int, Int>()
    private val toUpdateSongs = HashMap<Int, Int>()

    private val notificationTitles: HashMap<Int, String> = HashMap()
    private val notificationTexts: HashMap<Int, String> = HashMap()

    private var isFullScan = true

    private val retrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://genius.com/api/")
        .build()
    private val geniusService: GeniusService = retrofit.create(GeniusService::class.java)

    override fun doWork(): Result {

        createNotificationChannel()

        val artistId =  inputData.getInt(applicationContext.getString(R.string.extra_artist_id), -1)
        if(artistId != -1){
            isFullScan = false
        }

        // Get db components
        db = AppDatabase.getAppDatabase(applicationContext)
        artistDao = db.artistDao()
        songDao = db.songDao()
        contributionDao = db.contributionDao()


        songDao.getAll().forEach { song ->
            databaseSongs[song.id] = song.updated
        }

        if(isFullScan) {
            // Scan artists for updated songs
            val artists = artistDao.getAllSubscribed()
            val artistCount = artists.size
            artists.forEachIndexed { i, artist ->
                notifyProgress(NOTIF_ARTIST_PROGRESS_TITLE, artist.name, i, artistCount)
                processArtist(artist.id)
            }
        }
        else {
            processArtist(artistId)
        }

        // Notification progress index
        var j = 0

        // Process new songs
        val newSongsCount = toAddSongs.size
        toAddSongs.forEach { (id, updatedAt) ->
            val previousTitle = processSong(id, updatedAt, true)
            if (isFullScan) {
                notifyProgress(NOTIF_ADD_PROGRESS_TITLE, previousTitle, ++j, newSongsCount)
            }
        }

        // Update modified songs
        j = 0
        val updatedSongsCount = toUpdateSongs.size
        toUpdateSongs.forEach { (id, updatedAt) ->
            val previousTitle = processSong(id, updatedAt, false)
            if(isFullScan){
                notifyProgress(NOTIF_UPDATE_PROGRESS_TITLE, previousTitle, ++j, updatedSongsCount)
            }
        }

        if(isFullScan) {
            notifyResults(newSongsCount, updatedSongsCount)
        }
        println("Scan completed")
        return Result.success()
    }

    private fun processArtist(artistId : Int){
        val call = geniusService.releases(artistId)

        try {
            val response: Response<ReleaseApi> = call.execute()
            val apiResponse: ReleaseApi = response.body()!!

            val releases = apiResponse.response.songs

            releases.forEach { release ->
                if (databaseSongs[release.id] == null) {
                    toAddSongs[release.id] = release.updated_by_human_at
                } else if (databaseSongs[release.id]!! < release.updated_by_human_at) {
                    toUpdateSongs[release.id] = release.updated_by_human_at
                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    private fun processSong(id: Int, updatedAt: Int, isNew: Boolean): String {
        var returnTitle = ""

        songDao.delete(id)
        contributionDao.deleteForSongId(id)

        val call = geniusService.song(id)

        try {
            val response: Response<SongApi> = call.execute()
            val apiResponse: SongApi = response.body()!!

            val song = apiResponse.response.song

            returnTitle = song.title

            val songInsert = Song(
                song.id,
                song.title,
                song.song_art_image_url,
                song.release_date ?: applicationContext.getString(R.string.unknown_date),
                false,
                updatedAt,
                if (isNew) Song.State.NEW.code else Song.State.UPDATED.code
            )

            songDao.insert(songInsert)

            // Primary
            val p = song.primary_artist
            artistDao.insert(Artist(p.id, p.name, p.image_url, false))
            contributionDao.insert(
                Contribution(
                    song.id,
                    p.id,
                    Contribution.Companion.Role.RolePrimary.name
                )
            )

            // Featurings
            song.featured_artists?.forEach {
                artistDao.insert(Artist(it.id, it.name, it.image_url, false))
                contributionDao.insert(
                    Contribution(
                        song.id,
                        it.id,
                        Contribution.Companion.Role.RoleFeaturing.name
                    )
                )
            }

            // Producers
            song.producer_artists?.forEach {
                artistDao.insert(Artist(it.id, it.name, it.image_url, false))
                contributionDao.insert(
                    Contribution(
                        song.id,
                        it.id,
                        Contribution.Companion.Role.RoleProducer.name
                    )
                )
            }

            // Writers
            song.writer_artists?.forEach {
                artistDao.insert(Artist(it.id, it.name, it.image_url, false))
                contributionDao.insert(
                    Contribution(
                        song.id,
                        it.id,
                        Contribution.Companion.Role.RoleWriter.name
                    )
                )
            }

            if (isNew) {
                notificationTitles[song.id] = song.title
                notificationTexts[song.id] = p.name
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return returnTitle
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
            mChannel.description = CHANNEL_DESCRIPTION
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager =
                applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }
    }

    private fun notifyProgress(title: String, name: String, i: Int, total: Int) {
        val builder = NotificationCompat.Builder(applicationContext, CHANNEL_ID).apply {
            setSmallIcon(R.drawable.ic_sync_black_24dp)
            setContentTitle(name)
            setContentText(title)
            setOngoing(true)
            setProgress(total, i, false)
            priority = NotificationCompat.PRIORITY_DEFAULT
        }

        with(NotificationManagerCompat.from(applicationContext)) {
            notify(NOTIF_PROGRESS_ID, builder.build())
        }
    }

    private fun notifyResults(new: Int, updated: Int) {
        with(NotificationManagerCompat.from(applicationContext)) {
            cancel(NOTIF_PROGRESS_ID)
        }

        val text = prepareNotificationText(new, updated)

        // Group Notification
        val groupNotification = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
            .setGroup("GROUP_NEW_SONGS")
            .setGroupSummary(true)
            .setLargeIcon(
                BitmapFactory.decodeResource(
                    applicationContext.resources,
                    R.drawable.ic_album_black_24dp
                )
            )
            .setSmallIcon(R.drawable.ic_check_black_24dp)
            .setContentTitle("New songs")
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)


        with(NotificationManagerCompat.from(applicationContext)) {
            notify(NOTIF_COMPLETE_GROUP_ID, groupNotification.build())
        }

        notificationTitles.forEach { (id, title) ->

            val songActivityIntent = Intent(applicationContext, SongActivity::class.java)
            songActivityIntent.putExtra(applicationContext.getString(R.string.extra_song_id), id)
            songActivityIntent.action = "${System.currentTimeMillis()}"
            val pendingIntent = PendingIntent.getActivity(
                applicationContext,
                id,
                songActivityIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )


            val songNotification =
                NotificationCompat.Builder(applicationContext, CHANNEL_ID).apply {
                    setGroup("GROUP_NEW_SONGS")
                    setGroupSummary(false)
                    setLargeIcon(
                        BitmapFactory.decodeResource(
                            applicationContext.resources,
                            R.drawable.ic_album_black_24dp
                        )
                    )
                    setSmallIcon(R.drawable.ic_check_black_24dp)
                    setContentTitle(title)
                    setContentText(notificationTexts[id]!!)
                    priority = NotificationCompat.PRIORITY_DEFAULT
                    setAutoCancel(true)
                    setContentIntent(pendingIntent)
                }
                    .build()


            with(NotificationManagerCompat.from(applicationContext)) {
                notify(NOTIF_COMPLETE_BASE_ID + id, songNotification)
            }

        }

    }

    private fun prepareNotificationText(new: Int, updated: Int): String {
        if (new > 0 || updated > 0) {
            var text = ""
            if (new == 1) {
                text += "1 new song"
            } else if (new >= 2) {
                text += "$new new songs"
            }
            if (new > 0 && updated > 0) {
                text += ", "
            }
            if (updated == 1) {
                text += "1 song updated"
            } else if (updated >= 2) {
                text += "$new songs updated"
            }
            return text
        } else {
            return "No new songs"
        }
    }

    companion object {
        const val CHANNEL_NAME = "SCAN_CHANNEL"
        const val CHANNEL_DESCRIPTION = "Channel used to notify music releases"
        const val CHANNEL_ID = "444"

        const val NOTIF_PROGRESS_ID = 4
        const val NOTIF_COMPLETE_GROUP_ID = 8
        const val NOTIF_COMPLETE_BASE_ID = 9

        const val NOTIF_ARTIST_PROGRESS_TITLE = "Syncing artists"
        const val NOTIF_ADD_PROGRESS_TITLE = "New songs"
        const val NOTIF_UPDATE_PROGRESS_TITLE = "Modified songs"
        const val NOTIF_COMPLETE_TITLE = "Synchronization completed"
    }

}