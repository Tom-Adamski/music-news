package com.teczer.musicnews

import android.app.Application
import com.facebook.stetho.Stetho

class MusicNewsApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }
}