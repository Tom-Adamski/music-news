package com.teczer.musicnews

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.LinearLayoutCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.teczer.musicnews.database.AppDatabase
import com.teczer.musicnews.database.dao.ArtistDao
import com.teczer.musicnews.database.dao.ContributionDao
import com.teczer.musicnews.database.dao.SongDao
import com.teczer.musicnews.database.entity.Contribution
import kotlinx.android.synthetic.main.activity_song.*
import kotlinx.android.synthetic.main.card_artist.view.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class SongActivity : AppCompatActivity() {

    private lateinit var db : AppDatabase
    private lateinit var songDao : SongDao
    private lateinit var artistDao: ArtistDao
    private lateinit var contributionDao: ContributionDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_song)

        val songId = intent.getIntExtra(getString(R.string.extra_song_id), -1)

        db = AppDatabase.getAppDatabase(this)
            
        songDao = db.songDao()
        artistDao = db.artistDao()
        contributionDao = db.contributionDao()

        AsyncTask.execute {
            val song = songDao.getById(songId)

            runOnUiThread {
                song_title.text = song?.title
                if(song?.date != null) {
                    try {
                        //Display release date according to locale
                        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
                        val date = sdf.parse(song.date)
                        val dateFormat = android.text.format.DateFormat.getDateFormat(applicationContext)
                        release_date.text = dateFormat.format(date)
                        release_date.visibility = View.VISIBLE
                    }
                    catch (e : ParseException){
                        println("Failed to display release date.")
                    }
                }
            }

            runOnUiThread {
                Glide.with(applicationContext)
                    .load(song?.imageUrl)
                    .placeholder(R.drawable.ic_album_black_24dp)
                    .into(song_cover)
            }

            val contributions = contributionDao.getAllForSong(songId)

            contributions.forEach {

                val artist = artistDao.loadById(it.artistId)

                if(artist != null) {

                    var container : LinearLayoutCompat? = null

                    runOnUiThread {
                        container = LinearLayoutCompat(applicationContext)

                        val artistLayout = LayoutInflater.from(applicationContext).inflate(R.layout.card_artist, container)
                        artistLayout.name.text = artist.name
                        Glide.with(applicationContext)
                            .load(artist.imageUrl)
                            .placeholder(R.drawable.ic_person_black_24dp)
                            .apply(RequestOptions.circleCropTransform())
                            .into(artistLayout.photo)
                    }


                    when (it.role) {
                        Contribution.Companion.Role.RolePrimary.name -> {
                            runOnUiThread {
                                header_primary.visibility = View.VISIBLE
                                primary.addView(container)
                            }
                        }

                        Contribution.Companion.Role.RoleFeaturing.name -> {

                            runOnUiThread {
                                header_featuring.visibility = View.VISIBLE
                                featurings.addView(container)
                            }
                        }

                        Contribution.Companion.Role.RoleProducer.name -> {

                            runOnUiThread {
                                header_producer.visibility = View.VISIBLE
                                producers.addView(container)
                            }
                        }

                        Contribution.Companion.Role.RoleWriter.name -> {

                            runOnUiThread {
                                header_writer.visibility = View.VISIBLE
                                writers.addView(container)
                            }
                        }


                    }
                }
            }



        }


    }
}
