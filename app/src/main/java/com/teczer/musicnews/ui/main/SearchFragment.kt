package com.teczer.musicnews.ui.main

import android.os.AsyncTask
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.teczer.musicnews.R
import com.teczer.musicnews.adapter.SearchAdapter
import com.teczer.musicnews.api.ArtistApi
import com.teczer.musicnews.api.GeniusService
import com.teczer.musicnews.database.AppDatabase
import com.teczer.musicnews.database.dao.ArtistDao
import com.teczer.musicnews.database.entity.Artist
import com.teczer.musicnews.listener.OnArtistClickListener
import com.teczer.musicnews.workers.SyncWorker
import kotlinx.android.synthetic.main.fragment_search.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class SearchFragment : Fragment(), OnArtistClickListener {

    private lateinit var adapter: SearchAdapter

    private lateinit var db: AppDatabase
    private lateinit var artistDao: ArtistDao

    private val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://genius.com/api/")
        .build()
    private val geniusService = retrofit.create(GeniusService::class.java)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_search, container, false)

        db = AppDatabase.getAppDatabase(activity!!)
        artistDao = db.artistDao()

        adapter = SearchAdapter(context!!, this)

        root.recycler_view.adapter = adapter
        root.recycler_view.layoutManager = LinearLayoutManager(context!!)
        root.recycler_view.setHasFixedSize(true)
        root.recycler_view.setItemViewCacheSize(20)

        root.search_button.setOnClickListener {
            val name = root.search_query.text.toString()
            searchArtist(name)
        }

        root.search_query.setOnKeyListener { _, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                val name = root.search_query.text.toString()
                searchArtist(name)

                true
            } else {
                false
            }
        }

        return root
    }

    private fun searchArtist(name: String) {

        adapter.empty()

        val call = geniusService.artists(name)

        call.enqueue(object : Callback<ArtistApi>{
            override fun onResponse(call: Call<ArtistApi>, response: Response<ArtistApi>) {
                if(response.body() != null) {

                    val hits = response.body()!!.response.sections[0].hits

                    hits.forEach { hit ->
                        val artistId = hit.result.id
                        val artistName = hit.result.name
                        val imageUrl = hit.result.image_url
                        val artist = Artist(artistId, artistName, imageUrl, true)

                        activity!!.runOnUiThread {
                            adapter.addArtist(artist)
                        }
                    }

                }
                else{
                    showLoadingError()
                }
            }
            override fun onFailure(call: Call<ArtistApi>, t: Throwable) {
                showLoadingError()
            }
            fun showLoadingError(){
                Toast.makeText(context, "Error loading artists", Toast.LENGTH_LONG).show()
            }
        })
    }


    override fun onArtistClick(artist: Artist, action: OnArtistClickListener.ArtistAction) {

        if (action == OnArtistClickListener.ArtistAction.SUBSCRIBE) {
            AsyncTask.execute {
                artistDao.subscribe(artist)
                activity?.runOnUiThread {
                    Toast.makeText(context, "Subscribed to ${artist.name} !", Toast.LENGTH_SHORT)
                        .show()
                }
                val releaseWorker = OneTimeWorkRequestBuilder<SyncWorker>()
                val data = Data.Builder()
                data.putInt(getString(R.string.extra_artist_id), artist.id)
                releaseWorker.setInputData(data.build())
                WorkManager.getInstance(context!!).enqueue(releaseWorker.build())

            }
        }
    }
    
}