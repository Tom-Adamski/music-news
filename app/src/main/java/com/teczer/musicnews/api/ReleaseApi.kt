package com.teczer.musicnews.api

data class ReleaseApi(
    val response: ReleaseResponse
)

data class ReleaseResponse(
    val songs: List<ReleaseHit>
)

data class ReleaseHit(
    val id : Int,
    val title : String,
    val updated_by_human_at : Int,
    val song_art_image_url : String
)